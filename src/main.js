// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import './assets/reset.css' //清除默认样式
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import BaiduMap from 'vue-baidu-map'
import { Base64 } from 'js-base64'
import {postRequest, getRequest} from './api/index'//引入封装后的axios请求方法
import store from './store'
Vue.config.productionTip = false
Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: 'jyFGZNv9j8EBtSGa351HMIGKp7MxZPPW'
})
Vue.use(ElementUI);
Vue.use(Base64);
router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})
//将常用的请求挂载到vue实例上
Vue.prototype.postRequest = postRequest;
Vue.prototype.getRequest = getRequest;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
