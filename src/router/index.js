import Vue from 'vue'
import Router from 'vue-router'
// 首页
import Home from '@/pages/Home/Home'
// 网点管理
import WebsiteManage from '@/pages/WebsiteManage/WebsiteManage'
// 模版管理
import ArkTmp from '@/pages/ArkTmp/ArkTmp'
// 快件查询
import ExpressQuery from '@/pages/ExpressQuery/ExpressQuery'
// 老板查询
import BossManage from '@/pages/BossManage/BossManage'
// 登录
import Login from '@/pages/Login/Login'
// 网点详情
import WebsiteDetail from '@/pages/WebsiteDetail/WebsiteDetail'
// 模版详情
import TmpDetail from '@/pages/TmpDetail/TmpDetail'
// 快件详情
import ExpressDetail from '@/pages/ExpressDetail/ExpressDetail'
// 柜老板管理详情
import BossDetail from '@/pages/BossDetail/BossDetail'
// 绑定柜机
import Binding from '@/pages/Binding/Binding'
// 新增网点
import AddCabinet from '@/pages/AddCabinet/AddCabinet'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
     {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/website',
      name: 'WebsiteManage',
      component: WebsiteManage
    },
    {
      path: '/arkTmp',
      name: 'ArkTmp',
      component: ArkTmp
    },
    {
      path: '/expressQuery',
      name: 'ExpressQuery',
      component: ExpressQuery
    },
    {
      path: '/bossManage',
      name: 'BossManage',
      component: BossManage
    },
    {
    	path: '/websiteDetail',
      name: 'WebsiteDetail',
      component: WebsiteDetail
    },
    {
    	path: '/tmpDetail',
      name: 'TmpDetail',
      component: TmpDetail
    },
    {
    	path: '/expressDetail',
      name: 'ExpressDetail',
      component: ExpressDetail
    },
    {
    	path: '/bossDetail',
      name: 'BossDetail',
      component: BossDetail
    },
    {
    	path: '/binding',
      name: 'Binding',
      component: Binding
    },
    {
      path: '/addCabinet',
      name: 'AddCabinet',
      component: AddCabinet
    }
  ]
})
