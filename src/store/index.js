import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    userName: localStorage.userName || '华寓测试',
    exclude: localStorage.exclude || ['Home','WebsiteManage','ArkTmp','ExpressQuery','BossManage','WebsiteDetail','ExpressDetail',"BossDetail","Binding",'TmpDetail']
  },
  mutations: {
    login (state,userName) {
      state.userName = userName;
      localStorage.userName = userName;
    },
    keepalive (state,exclude) {
      state.exclude = exclude;
      localStorage.exclude = exclude;
    }
  }
})