import axios from 'axios'

// let base = 'http://134.175.84.200:13188/cabinet-manager-web/api/'
let base = 'http://47.107.33.35:13188/cabinet-manager-web/api/'

axios.interceptors.request.use((config) => {
  return config
}, (err) => {
  alert('请求超时')
  return Promise.resolve(err)
})


axios.interceptors.response.use((data) => {
  if(data.data.code != 10000) {
    alert(data.data.msg?data.data.msg:'服务器异常，请稍后重试');
    return;
  }
  else {
    return data
  }
  
}, (err) => {
   // 数据异常统一处理
  if (err) {
    alert('服务器被吃了,请刷新页面后重试或联系管理员')
  } 
})


export function postRequest(url, params) {
  return axios({
    method: 'post',
    url: `${base}${url}`,
    data: params,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

export function uploadFileRequest(url, params) {
  return axios({
    method: 'post',
    url: `${base}${url}`,
    data: params,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export function putRequest(url, params) {
  return axios({
    method: 'put',
    url: `${base}${url}`,
    data: params,
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

export function deleteRequest(url) {
  return axios({
    method: 'delete',
    url: `${base}${url}`
  })
}

export function getRequest(url) {
  return axios({
    method: 'get',
    url: `${base}${url}`
  })
} 